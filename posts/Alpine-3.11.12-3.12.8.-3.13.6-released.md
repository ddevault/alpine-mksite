---
title: 'Alpine 3.11.12, 3.12.8 and 3.13.6 released'
date: 2021-08-31
---

Alpine 3.11.12, 3.12.8 and 3.13.6 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version [3.11.12](https://git.alpinelinux.org/aports/log/?h=v3.11.12),
[3.12.8](https://git.alpinelinux.org/aports/log/?h=v3.12.8) and
[3.13.6](https://git.alpinelinux.org/aports/log/?h=v3.13.6) of its Alpine
Linux operating system.

Those releases include fixes for apk-tools [CVE-2021-36159](https://security.alpinelinux.org/vuln/CVE-2021-36159)
and openssl [CVE-2021-3711](https://security.alpinelinux.org/vuln/CVE-2021-3711) and [CVE-2021-3712](https://security.alpinelinux.org/vuln/CVE-2021-3712).


