---
title: 'Switching to OFTC'
date: 2021-05-19
---

Switching to OFTC
=================

Due to [recent changes concerning operational control of freenode][freenode-faq],
we have decided to move the Alpine community channels to OFTC.

We would like to thank the previous freenode management team for their work in
making a friendly space for the Alpine community, and wish them luck with their
[new IRC network project][libera-chat].

Please adjust your IRC clients to use `irc.oftc.net` when accessing the `#alpine-devel`
and `#alpine-linux` channels.  We apologize for any inconvience caused by this
change.

   [freenode-faq]: https://gist.github.com/joepie91/df80d8d36cd9d1bde46ba018af497409
   [libera-chat]: https://libera.chat
