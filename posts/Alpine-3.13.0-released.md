---
title: 'Alpine 3.13.0 released'
date: 2021-01-14
---

Alpine Linux 3.13.0 Released
===========================

We are pleased to announce the release of Alpine Linux 3.13.0, the first in
the v3.13 stable series.

New features and noteworthy new packages
----------------------------------------

* Official [cloud images](../cloud).
* Introduction of [ifupdown-ng](https://github.com/ifupdown-ng/ifupdown-ng),
  a replacement for busybox ifupdown.
* Improved wifi support in setup scripts
* PHP 8.0 is available now (next to PHP 7.4).
* Node.js (LTS) is compiled with -O2 instead of -Os which noticeably improves
  performance. It can also use full ICU data if new package icu-data is
  installed alongside.
* Initial support for cloud-init

Significant updates
-------------------

* Linux 5.10.7
* musl 1.2
* Busybox 1.32.1
* GCC 10.2.1
* Git 2.30.0
* Knot DNS 3.0.3
* MariaDB 10.5.8
* Node.js 14.15.4
* Nextcloud 20.0.4
* PostgreSQL 13.1
* QEMU 5.2.0
* Xen 4.14.1
* Zabbix 5.2.3
* ZFS 2.0.1

Upgrade notes
---------------------

* The musl-1.2 upgrade changed the definition of `time_t` to 64-bits on all
  arches. This affects armhf, armv7 and x86. See the [musl time64 release notes][11]
  and the [wiki][8] for more information.
* Berkley DB has been deprecated due to licensing issues. For postfix, this
  means all tables in `/etc/postfix/main.cf` must be converted to a different
  format before upgrading, for example, `lmdb`.
* xorg-server and related packages have been moved to community. Make sure you
  have the community repository enabled in `/etc/apk/repositories`.
* Some rarely used busybox applets have been disabled. See the
  [wiki][8] for more information.
* In order to upgrade to Nextcloud 20, the nextcloud19 package can be used as
  an intermediate version.

Credits
-------

Thanks to everyone sending in patches, bug reports, new and updated aports,
and to everyone helping with writing documentation, maintaining the
infrastructure, or has contributed in any other way!

Thanks to [GIGABYTE][1], [Linode][2], [Fastly][3], [IBM][4], [Packet][5],
[vpsFree][6] and [RapidSwitch][7] for providing us with hardware and
hosting.

Changes
-------

The full list of changes can be found in the [wiki][8],  [git log][9] and [bug tracker][10].


[1]: http://b2b.gigabyte.com/
[2]: https://linode.com
[3]: https://www.fastly.com/
[4]: https://ibm.com/
[5]: https://packet.net/
[6]: https://vpsfree.org
[7]: https://www.rapidswitch.com/
[8]: https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.13.0
[9]: https://git.alpinelinux.org/cgit/aports/log/?h=v3.13.0
[10]: https://gitlab.alpinelinux.org/alpine/aports/issues?scope=all&utf8=%E2%9C%93&state=closed&milestone_title=3.13.0
[11]: https://musl.libc.org/time64.html


Commit statistics
-----------------

<pre>
    18	6543
     9	Adam Jensen
     2	Adrian L Lange
     3	Al-Hassan Abdel-Raouf
     2	Aleks Bunin
     1	Alex
    22	Alex Denes
    27	Alex McGrath
     2	Alex Raschi
     7	Alex Xu (Hello71)
     1	Alexander Köplinger
     2	Andreas Bertsch
     1	Andreas Pachler
     1	Andrew Starr-Bochicchio
     1	Andrey
    20	André Klitzing
   356	Andy Postnikov
    11	Anjandev Momi
     1	Anri Dellal
    32	Antoine Fontaine
     8	Antoni Aloy Torrens
   375	Ariadne Conill
     2	Artur Mądrzak
     1	Atsushi Watanabe
     1	Axel Burri
   756	Bart Ribbers
     1	Blake Oliver
     2	Boris Faure
     1	Bradford D. Boyle
     1	Brian Davis
     5	Bridouz
     3	Carl Chave
     2	Carlo Landmeter
     3	Charles Wimmer
    55	Clayton Craft
     1	Cory Sanin
    28	Curt Tilmes
     2	Cían Hughes
     5	Damian Kurek
    16	Danct12
     4	Daniel Gray
    29	Daniel Néri
     3	Daniel Santana
     1	Daniell Crossman
     1	Dave Henderson
    44	David Demelier
    10	David Florness
     6	Dekedro
    41	Dennis Krupenik
     1	Dennis Vestergaard Værum
     2	Dermot
    46	Dermot Bradley
     1	Devin Lin
     2	Diaz Urbaneja Victor Diego Alenjandro
     6	Dmitry Romanenko
     1	Dolphin von Chips
    48	Drew DeVault
    76	Duncan Bellamy
     2	Dylan Van Assche
     1	Edward Xia
     2	Emanuele Sorce
     1	Eric Poelke
     8	Eric Tian
    11	Erik Larsson
     1	Erik Wisuri
    27	Fabian Affolter
   538	Francesco Colista
     2	Francisco Gonzalez
     3	Frank Oltmanns
     3	Fraser Waters
     1	Fredrik Lönnegren
    80	Galen Abell
     2	Gennady Feldman
     7	Geod24
     3	George Hopkins
     1	George Vanburgh
     2	Gergo Huszty
     1	Gompa
     4	Gustavo L F Walbon
     1	Gustavo Walbon
     2	Haelwenn (lanodan) Monnier
    30	Henrik Riomar
     2	Herve Commowick
     1	Hiroshi Kajisha
    41	Holger Jaekel
     5	Ian Bashford
     2	Iggy Jackson
     1	Isaiah Inuwa
     1	Iskren Chernev
   998	J0WI
     1	Jacek Migacz
     2	Jack O'Sullivan
     3	Jacob Gulotta
    24	Jake Buchholz
   306	Jakub Jirutka
     1	James Holcomb
     4	Jami Kettunen
     1	Jan Segre
     1	Jean-Louis Fuchs
     3	Jinming Wu, Patrick
     2	Joel H
     1	Joel Hansen
     2	Johannes Heimansberg
     1	Johannes Müller
     1	John Boehr
     1	Joonas Kuorilehto
     1	Jordan Christiansen
     1	Josef Vybíhal
     2	Julian Weigt
   779	Justin Berthault
     1	Justin Schneck
     2	Jürgen Brunink
    24	Kaarle Ritvanen
    16	Keith Maxwell
   271	Kevin Daudt
    76	Konstantin Kulikov
     1	Konstantin Tcepliaev
     2	Kuichi Fujiwara
     1	Kurkodejm
     4	Larry Reaves
     1	Laszlo Soos
    20	Laurent Bercot
  2915	Leo
    35	Leon Marz
    75	Leonardo Arena
     1	Liam Nattrass
    31	Luca Weiss
     8	Lucas Ramage
     4	Maarten van Gompel
     1	Manuel
     1	Manuel Mendez
     1	Marcin Konicki
     1	Marcus Rohrmoser
    79	Marian Buschsieweke
     1	Mark Hills
     1	Mark Pashmfouroush
     3	Martijn Braam
     1	Martin Mauch
     5	Martin Schmidt
     2	Marvin Steadfast
     2	Mathew Meins
     1	MathxH Chen
    34	Matthew T Hoare
     1	Matthias Neugebauer
     3	Maurice Zhou
     5	Mengyang Li
     1	Michael Aldridge
     1	Michael Kirsch
    29	Michael Pirogov
     3	Michael Truog
     1	Michal Artazov
    31	Michał Adamski
   571	Michał Polański
    12	Mike Crute
     1	Mike Sullivan
   321	Milan P. Stanić
    13	Miles Alan
     2	Minecrell
     3	Mogens Jensen
     2	Mohammad Abdolirad
   972	Natanael Copa
     1	Nathan Angelacos
     4	Nathan Owens
     3	Naveen N. Rao
     1	Nayana Hettiarachchi
    53	Newbyte
     1	Nick O'Connor
     1	Nico Schottelius
     1	Niek van der Maas
     1	Niklas Cathor
    13	Noel Kuntze
    62	Oleg Titov
    37	Oliver Smith
     1	Olivier Moreau
    22	Olliver Schinagl
     1	Otto Modinos
     1	Paolo Bonzini
     1	Paper
     3	Patrick Gansterer
    10	Paul Bredbury
     6	Pedro Filipe
     1	Pellegrino Prevete
    22	PureTryOut
     2	R4SAS
     3	Raatty
     1	Rahul Atlury
  1081	Rasmus Thomsen
     3	Raymond Page
     1	Reed Wade
     1	Richard Kojedzinszky
     2	Richard Patel
     1	Rodrigo Lourenço
     4	Rosen Penev
     1	Rumi
     7	Russ Webber
     3	Sam Stuewe
     6	Sean McAvoy
     3	Sergey Fukanchik
     1	Shyam Sunder
     1	Simon Engledew
    39	Simon Frankenberger
     8	Simon Rupf
     1	Simon Zeni
     8	Sodface
     1	Sora Morimoto
    12	Stefan Reiff
     1	Steffen Lange
     2	Steven Honson
     4	Stuart Cardall
     1	Sylvain Emery
   183	Sören Tempel
    91	TBK
    15	Taner Tas
     1	Taras Zaporozhets
     3	Ted Trask
     7	Terror
     1	Thibault Ferrante
     6	Thomas Deutsch
     8	Thomas Kienlen
    35	Thomas Liske
     5	Tim Brust
     1	Tim Magee
    20	Timo Teräs
     6	Timothee LF
   122	Timothy Legge
     1	Timothée Floure
     1	Tiziano Müller
     1	Tobias Genannt
     4	Trung Le
    13	Trung Lê
     5	Tuan Hoang
     5	Ty Sarna
     1	Veovis
     2	Victor Diego Alejandro Diaz Urbaneja
     1	Victor Itkin
     1	Victor Schröder
    51	Will Sinatra
     1	William Desportes
     1	William Johansson
     2	Winston Weinert
     3	Yohann D'ANELLO
     1	ant
     4	antonialoytorrens
     4	asdf
     1	atka
     1	bds
     1	camerden
     1	delphi
     1	dzs0geiyae6ohyee.gitmask@pay.pizza
     1	ewisuri
     1	ignapk
     1	j3s
     3	jvoisin
     1	khanku
    10	kpcyrd
     2	lflare
     2	llnu
     1	macmpi
     2	mimi89999
     1	mrl5
     3	nick
    31	nick black
     2	nixfloyd
     1	opal hart
    19	pglaum
   256	prspkt
    21	rahmanshaber
     2	rfaa
     1	scudco
     4	solidnerd
     1	takumin
    10	techknowlogick
     3	thibault.ferrante
    29	wener
     1	xrs
     1	zhengml
     1	Éloi Rivard
     1	Ömer Faruk IRMAK
     1	Đoàn Trần Công Danh
</pre>
