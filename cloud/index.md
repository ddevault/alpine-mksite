<div class="pure-g">

    <div class="pure-u-1">
        <div class="l-box">
            <h1>Cloud Images</h1>
            <p>The following providers are available:</p>
            <ul>
              <li><a href="#aws-ec2">AWS EC2</a></li>
              <li><a href="#builds.sr.ht">builds.sr.ht</a></li>
            </ul>
        </div>
    </div>

    <div class="pure-u-1">
      <div class="l-box">
        <h2 id="aws-ec2">AWS EC2</h2>
        <p>
            The login user for the images is <b>alpine</b>. SSH keys for
            this user will be installed from the instance metadata service.
        </p>
      </div>
    </div>

    {{#cloud/releases.clouds.aws}}
    <div class="pure-u-1">
        <div class="cloud">
            <h3>{{version}}</h3>
            <table class="pure-table pure-table-bordered">
                <thead>
                    <tr>
                        <th>Provider</th>
                        <th>Region</th>
                        <th>Architecture</th>
                        <th>Released</th>
                        <th>EOL Date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {{#releases}}
                {{#artifacts}}
                    <tr>
                        <td>AWS EC2</td>
                        <td><a href="https://{{region}}.console.aws.amazon.com/ec2/home#Images:visibility=public-images;imageId={{image_id}}">{{region}}</a></td>
                        <td>{{arch}}</td>
                        <td>{{creation_date}}</td>
                        <td>{{end_of_life}}</td>
                        <td>
                            <a href="https://{{region}}.console.aws.amazon.com/ec2/home#launchAmi={{image_id}}" class="green-button">
                                <i class="fa fa-download"></i>&nbsp;Launch
                            </a>
                        </td>
                    </tr>
                {{/artifacts}}
                {{/releases}}
                </tbody>
            </table>
        </div>
    </div>
    {{/cloud/releases.clouds.aws}}

    <div class="pure-u-1">
      <div class="l-box">
          <h2 id="builds.sr.ht">builds.sr.ht</h2>
          <p>
            Pre-configured build manifests are available which launch a
            temporary Alpine Linux environment accessible via SSH.
          </p>
      </div>
    </div>

    <div class="pure-u-1">
        <div class="cloud">
            <table class="pure-table pure-table-bordered">
                <thead>
                    <tr>
                        <th>Alpine Version</th>
                        <th>Architecture</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                  <tr>
                      <td>Alpine edge</td>
                      <td>x86_64</td>
                      <td>
                          <a href="https://builds.sr.ht/submit?manifest=%23%20Press%20%22submit%22%20to%20boot%20a%20temporary%20Alpine%20Linux%20VM%0Aimage%3A%20alpine%2Fedge%0Ashell%3A%20true" class="green-button">
                              <i class="fa fa-download"></i>&nbsp;Launch
                          </a>
                      </td>
                  </tr>
                  <tr>
                      <td>Alpine 3.14</td>
                      <td>x86_64</td>
                      <td>
                          <a href="https://builds.sr.ht/submit?manifest=%23%20Press%20%22submit%22%20to%20boot%20a%20temporary%20Alpine%20Linux%20VM%0Aimage%3A%20alpine%2F3.14%0Ashell%3A%20true" class="green-button">
                              <i class="fa fa-download"></i>&nbsp;Launch
                          </a>
                      </td>
                  </tr>
                  <tr>
                      <td>Alpine 3.13</td>
                      <td>x86_64</td>
                      <td>
                          <a href="https://builds.sr.ht/submit?manifest=%23%20Press%20%22submit%22%20to%20boot%20a%20temporary%20Alpine%20Linux%20VM%0Aimage%3A%20alpine%2F3.13%0Ashell%3A%20true" class="green-button">
                              <i class="fa fa-download"></i>&nbsp;Launch
                          </a>
                      </td>
                  </tr>
                  <tr>
                      <td>Alpine 3.12</td>
                      <td>x86_64</td>
                      <td>
                          <a href="https://builds.sr.ht/submit?manifest=%23%20Press%20%22submit%22%20to%20boot%20a%20temporary%20Alpine%20Linux%20VM%0Aimage%3A%20alpine%2F3.12%0Ashell%3A%20true" class="green-button">
                              <i class="fa fa-download"></i>&nbsp;Launch
                          </a>
                      </td>
                  </tr>
                  <tr>
                      <td>Alpine 3.11</td>
                      <td>x86_64</td>
                      <td>
                          <a href="https://builds.sr.ht/submit?manifest=%23%20Press%20%22submit%22%20to%20boot%20a%20temporary%20Alpine%20Linux%20VM%0Aimage%3A%20alpine%2F3.11%0Ashell%3A%20true" class="green-button">
                              <i class="fa fa-download"></i>&nbsp;Launch
                          </a>
                      </td>
                  </tr>
                </tbody>
            </table>
        </div>
    </div>

</div> <!-- end download -->
