out := _out
md_sources := $(wildcard *.md) $(wildcard [a-z]*/*.md)
pages := $(patsubst %.md,$(out)/%.html, $(md_sources))

static_sources := $(shell find _static -type f)
static_out := $(patsubst _static/%,$(out)/%,$(static_sources))

LUA ?= lua5.3
generate_page = $(LUA) _scripts/generate_page.lua
generate_index = $(LUA) _scripts/generate_index.lua
generate_news = $(LUA) _scripts/generate_news.lua
generate_downloads = $(LUA) _scripts/generate_downloads.lua
generate_cloud = $(LUA) _scripts/generate_cloud.lua
generate_commits = $(LUA) _scripts/generate_git-commits.lua
generate_atom = $(LUA) _scripts/generate_atom.lua
generate_releases_json = $(LUA) _scripts/generate_releases_json.lua
generate_release_branches = $(LUA) _scripts/generate_release_branches.lua

git_atom_url := https://git.alpinelinux.org/cgit/aports/atom

archs := $(shell $(LUA) -e  \
	'print(require("lyaml").load(io.read( "*a" ))[2].arches)' \
	< alpine-releases.conf.yaml)

latest_releases_yaml = $(archs:%=latest-releases.%.yaml)
releases_url := http://dl-master.alpinelinux.org/alpine/latest-stable/releases
releases_url_suffix = $(@:latest-releases.%.yaml=%/latest-releases.yaml)
cloud_releases_url := https://gitlab.alpinelinux.org/alpine/cloud/alpine-ec2-ami/-/raw/master/releases/alpine.yaml

all: $(pages) $(static_out) $(out)/atom.xml $(out)/releases.json

$(out)/index.html: git-commits.yaml news.yaml
$(out)/downloads/index.html: downloads/releases.yaml
$(out)/cloud/index.html: cloud/releases.yaml
$(out)/posts/index.html: posts/index.yaml
$(out)/releases/index.html: releases/release-branches.yaml
$(out)/conf/index.html: conf/schedule.yaml

$(out)/%.html: %.md _default.template.html
	mkdir -p $(dir $@)
	$(generate_page) $< $(filter %.yaml,$^) > $@.tmp
	mv $@.tmp $@

$(static_out): $(out)/%: _static/%
	mkdir -p $(dir $@)
	cp $< $@

%/index.yaml: %/*.md
	$(generate_index) $(filter %.md,$^) > $@.tmp
	mv $@.tmp $@

clean:
	rm -f $(pages) $(static_out) \
		$(latest_releases_yaml) downloads/releases.yaml \
		cloud/releases.yaml \
		git-commits.yaml \
		news.yaml posts/index.yaml \
		$(out)/atom.xml

$(latest_releases_yaml): alpine-releases.conf.yaml
	curl -L -J $(releases_url)/$(releases_url_suffix) > $@.tmp
	mv $@.tmp $@

cloud/releases-in.yaml:
	curl -L -J $(cloud_releases_url) > $@.tmp
	mv $@.tmp $@

downloads/releases.yaml: $(latest_releases_yaml) _scripts/generate_downloads.lua
	$(generate_downloads) $(filter %.yaml,$^) > $@.tmp && mv $@.tmp $@

cloud/releases.yaml: cloud/releases-in.yaml _scripts/generate_cloud.lua
	$(generate_cloud) cloud/releases-in.yaml > $@.tmp && mv $@.tmp $@

update-release:
	rm -f $(latest_releases_yaml) downloads/releases.yaml
	$(MAKE)

update-cloud:
	rm -f cloud/releases-in.yaml
	$(MAKE)

git-commits.yaml: _scripts/generate_git-commits.lua
	curl -L $(git_atom_url) | $(generate_commits) > $@.tmp
	mv $@.tmp $@

update-git-commits:
	rm -f git-commits.yaml
	$(MAKE)

posts/index.yaml: _scripts/generate_index.lua

releases/release-branches.yaml: alpine-releases.conf.yaml _scripts/generate_release_branches.lua
	$(generate_release_branches) $< > $@.tmp && mv $@.tmp $@

news.yaml: posts/index.yaml _scripts/generate_news.lua
	$(generate_news) < $< > $@.tmp
	mv $@.tmp $@

$(out)/atom.xml: news.yaml
	$(generate_atom) _atom.template.xml $< > $@.tmp
	mv $@.tmp $@

$(out)/releases.json: alpine-releases.conf.yaml _scripts/generate_releases_json.lua
	$(generate_releases_json) alpine-releases.conf.yaml > $@.tmp
	mv $@.tmp $@

DOCKER_IMAGE = alpine-mksite
.PHONY: docker-build
docker-build:
	docker build -t $(DOCKER_IMAGE) .

.PHONY: docker-run
docker-run: docker-build
	docker run -t -p 8000:8000 $(DOCKER_IMAGE)

.PHONY: docker-stop
docker-stop:
	docker ps | grep '8000->8000' | tr -s " " | cut -f 1 -d " " \
	  | xargs docker rm -f
