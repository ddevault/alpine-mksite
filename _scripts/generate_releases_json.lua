local yaml = require('lyaml')
local json = require('cjson')

local function filecontent(filename)
	local file=assert(io.open(filename, "r"))
	local content = file:read ("*a")
	file:close()
	return content
end

local function assert_relbranch(index, relbranch, field)
		assert(relbranch[field], ("%s is missing for %s"):format(field, relbranch.rel_branch or ("index %s"):format(index)))
end

-- scan all release branches and split the arches string into an array
-- returns a sorted list of all architectures found
local function split_arches(relbranches)
	local all_arches = {}
	local sorted_arches = {}
	local i, b
	for i, b in ipairs(relbranches) do
		local arches = {}
		assert_relbranch(i, b, "arches")
		for arch in b.arches:gmatch("%S+") do
			all_arches[arch] = true
			table.insert(arches, arch)
		end
		table.sort(arches)
		-- replace the string with an array
		b.arches = arches
	end
	-- array with the keys of arches
	for i in pairs(all_arches) do
		table.insert(sorted_arches, i)
	end
	table.sort(sorted_arches)

	return sorted_arches
end

-- scan all releases and get the release date from the release notes header
local function get_release_dates(relbranches)
	local i, b
	for i, b in ipairs(relbranches) do
		local release
		for _, release in ipairs(b.releases or {}) do
			assert(release.version, ("%s has a release without version (index %i)"):format(b.rel_branch, i))
			if not release.date then
				assert(release.notes, ("release %s is missing notes or date"):format(release.version))
				local data = filecontent(release.notes)
				header = yaml.load(data:match("^(%-%-%-.-%-%-%-)"))
				release.date = header.date
			end
		end
	end
end

local relbranches = yaml.load(filecontent(arg[1]))

all_arches = split_arches(relbranches)
get_release_dates(relbranches)

io.stdout:write(json.encode({
	architectures = all_arches,
	release_branches = relbranches
}))
